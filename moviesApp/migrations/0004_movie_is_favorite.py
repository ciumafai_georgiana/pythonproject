# Generated by Django 2.1.3 on 2019-01-03 22:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('moviesApp', '0003_auto_20190103_2345'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='is_favorite',
            field=models.BooleanField(default=False),
        ),
    ]
