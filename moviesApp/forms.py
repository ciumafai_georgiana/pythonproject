from django import forms
from django.contrib.auth.models import User
from .models import Movie, Genre

class GenreForm(forms.ModelForm):
    #clasa Meta contine informatii despre clasa
    class Meta:
        model = Genre
        fields = ['genreName']


class MovieForm(forms.ModelForm):

    class Meta:
        model = Movie
        fields = ['title', 'rating', 'releasedate', 'description']

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']