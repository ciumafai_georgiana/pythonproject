from django.db import models

class Genre(models.Model):
    genreName=models.CharField(max_length=250)
    def __str__(self):
        return self.genreName
	
class Movie(models.Model):
    genre=models.ForeignKey(Genre, on_delete=models.CASCADE)
    title=models.CharField(max_length=250)
    logo=models.CharField(max_length=1000)
    rating=models.CharField(max_length=250)
    description=models.CharField(max_length=500)
    releasedate=models.DateField(max_length=250)
    is_favorite= models.BooleanField(default=False)
	
    def __str__(self):
        return self.title