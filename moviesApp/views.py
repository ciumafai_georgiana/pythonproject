from django.shortcuts import render, get_object_or_404,redirect
from .models import Genre, Movie
from .forms import GenreForm, MovieForm, UserForm
from django.http import Http404, HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout

def homepage(request):
    all_genres=Genre.objects.all()
    all_movies=Movie.objects.all()
    return render(request, 'movies/homepage.html', {'all_genres': all_genres, 'all_movies': all_movies})

def genres(request):
    all_genres=Genre.objects.all()
    return render(request, 'movies/list_all.html', {'all_genres': all_genres})

def genreMovies(request, genre_id):
    genre=get_object_or_404(Genre, pk= genre_id)
    return render(request, 'movies/genre_movies.html', {'genre': genre})
    
def favorite(request, genre_id):
    genre=get_object_or_404(Genre, pk= genre_id)
    try: 
	    selected_movie = genre.movie_set.get(pk=request.POST['movie'])
    except (KeyError, Movie.DoesNotExist):
	    return render(request,'movies/genre_movies.html', {
		'genre': genre,
		'error_message': "You did not select a valid movie",
		})
    else: 
	    selected_movie.is_favorite = True
	    selected_movie.save()
	    return render(request, 'movies/genre_movies.html', {'genre': genre})

def movies(request):
    all_movies=Movie.objects.all()
    return render(request, 'movies/movies.html', {'all_movies': all_movies,})
	
def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('movies:homepage')
            else:
                return render(request, 'movies/login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'movies/login.html', {'error_message': 'Invalid login'})
    return render(request, 'movies/login.html')

def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'movies/login.html', context)
	
def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('movies:homepage')
    context = {
        'form': form,
    }
    return render(request, 'movies/register.html', context)

def create_genre(request):
    #if not request.user.is_authenticated():
    #    return render(request, 'movies/login.html')
    #else:
        form = GenreForm(request.POST or None, request.FILES or None)
        genres=Genre.objects.all()
        if form.is_valid():
            genre = form.save(commit=False)
            genreName=form.cleaned_data['genreName']
            for g in genres:
                if g.genreName ==  form.cleaned_data.get("genreName"):
                    context = {
                        'form': form,
						'error_message': 'The genre is already added',
					}
                    return render(request, 'movies/create_genre.html', context)
            genre.save()
            if genre is not None:
                return redirect('movies:genres')
        context = {
            'form': form,
        }
        return render(request, 'movies/create_genre.html', context)

def add_movie(request, genre_id):
    form = MovieForm(request.POST or None)
    genre = get_object_or_404(Genre, pk=genre_id)
    if form.is_valid():
        genre_movies = genre.movie_set.all()
        movie = form.save(commit=False)
        movie.genre = genre
        movie.title = form.cleaned_data['title']
        for s in genre_movies:
            if s.title == form.cleaned_data.get("title"):
                context = {
                    'genre': genre,
                    'form': form,
                    'error_message': 'You already added that movie',
                }
                return render(request, 'movies/create_movie.html', context)
       
        movie.save()
        if movie is not None:
                return redirect('movies:movies')
    context = {
        'genre': genre,
        'form': form,
    }
    return render(request, 'movies/create_movie.html', context)	
	
def search(request):
    if request.method=="POST":
	    search_text=request.POST['search']
    else:
        search_text=''
    movies=Movie.objects.filter(title__contains=search_text)
    return render(request, 'layout/header.html', {'movies':movies})
	
def get_movie(request, movie_id):
    movie=get_object_or_404(Movie, pk= movie_id)
    return render(request, 'movies/get_movies.html', {'movie': movie})
