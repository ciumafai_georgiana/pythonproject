from django.conf.urls import url
from . import views

app_name='movies'

urlpatterns = [
	#movies
	url(r'^$', views.homepage, name='homepage'),
	
    #/movies/genres/
	url(r'^genres/', views.genres, name='genres'),
	
	#/movies/id/
	url(r'^(?P<genre_id>[0-9]+)/$', views.genreMovies, name='genres'),
	
	#/movies/favorite/
	url(r'^(?P<genre_id>[0-9]+)/favorite/$', views.favorite, name='favorite'),
	
	#/movies/movies/
	url(r'^movies/', views.movies, name='movies'),
	
	#/movies/movie/<movie_id>
	url(r'^movie/(?P<movie_id>[0-9]+)/$', views.get_movie, name='get_movie'),
	
	#/movies/login/
	url(r'^login/', views.login_user, name='login_user'),
	
	#/movies/logout/
	url(r'^logout/', views.logout_user, name='logout_user'),
	
	#/movies/register/
	url(r'^register/', views.register, name='register'),
	
	#movies/create_genre
	url(r'^create_genre/', views.create_genre, name='create_genre'),
	
	#movies/<genre_id>/add_movie
	url(r'^(?P<genre_id>[0-9]+)/add_movie/$', views.add_movie, name='add_movie'),
	
	#movies/search
	url(r'^search/', views.search, name='search'),
	
]